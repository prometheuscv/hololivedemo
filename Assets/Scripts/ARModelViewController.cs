﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ARModelViewController : MonoBehaviour, IDragHandler
{
    public GameObject modelParent;
    private bool scaleChange;
    public bool isFirstFrameWithTwoTouches;

    private float mouseScrollWheelScale = 0.1f;
    private float rotateSpeed = 0.5f;

    public float scaleMax;
    public float scaleMin;

    public float cachedTouchDistance;
    public float cachedAugmentationScale;

    public float deltaX;
    public void Start() {

    }



    void Update()
    {
        if (Input.touchCount == 2)
        {

            float currentTouchDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            if (isFirstFrameWithTwoTouches)
            {
                scaleChange = true;
                cachedTouchDistance = currentTouchDistance;
                isFirstFrameWithTwoTouches = false;
            }
            float scaleMultiplier = currentTouchDistance / cachedTouchDistance;
            Debug.Log("scaleMultiplier:"+ scaleMultiplier);
            //scaleMultiplier = Mathf.Clamp(scaleMultiplier, scaleMin, scaleMax);
            float scaleAmount = Mathf.Clamp(cachedAugmentationScale * scaleMultiplier, scaleMin, scaleMax) ;
            modelParent.transform.localScale = new Vector3(scaleAmount, scaleAmount, scaleAmount);
        }
        else
        {
            scaleChange = false;
            isFirstFrameWithTwoTouches = true;
            cachedTouchDistance = 0;
            cachedAugmentationScale = modelParent.transform.localScale.x;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (modelParent.transform.localScale.x < scaleMax)
            {
                modelParent.transform.localScale += Vector3.one * mouseScrollWheelScale;
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (modelParent.transform.localScale.x > scaleMin)
            {
                modelParent.transform.localScale -= Vector3.one * mouseScrollWheelScale;
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!scaleChange)
        {
            deltaX = eventData.delta.x;
            modelParent.transform.localEulerAngles += new Vector3(0, -deltaX * rotateSpeed, 0);
        }
    }
}
