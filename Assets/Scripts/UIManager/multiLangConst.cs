﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class multiLangConst
{

    static Dictionary<string, string> LangMap_en = new Dictionary<string, string>{
        {"scene_button", "Scene"},
          {"scene_button_item_1", "SpaceShip"},
            {"scene_button_item_2", "Bar"},
             {"shader_button", "Shader"},
              {"shader_button_item_1", "Default"},
               {"shader_button_item_2", "Normal"},
                {"shader_button_item_3", "Holo"},
                   {"url_button", "URL"},
                    {"url_button_enter", "Enter"},
                     {"HIdeUI", "HIdeUI"},
                      {"ResetTrack", "ResetTrack"},
                         {"waitInfo", "Live streaming is loading, please wait"},
                          {"waitError", "The network is abnormal, the live streaming fails to load"},
                          {"fullScreen", "FullScreen"}
    };

    static Dictionary<string, string> LangMap_cn = new Dictionary<string, string>{
        {"scene_button", "场景"},
          {"scene_button_item_1", "太空船"},
            {"scene_button_item_2", "酒吧"},
              {"shader_button", "渲染"},
              {"shader_button_item_1", "正常"},
               {"shader_button_item_2", "法线"},
                {"shader_button_item_3", "科幻"},
                   {"url_button", "地址"},
                    {"url_button_enter", "进入"},
                     {"HIdeUI", "隐藏UI"},
                      {"ResetTrack", "重新定位"},
                          {"waitInfo", "直播加载中,请稍等..."},
                          {"waitError", "网络异常，直播加载失败"},
                             {"fullScreen", "全屏"}
    };

    public static string GetValue(string languageType, string key)
    {
        if (languageType == "en")
        {
            return LangMap_en[key];
        }
        else if (languageType == "cn")
        {
            return LangMap_cn[key];
        }
        return "N/A";
    }
}