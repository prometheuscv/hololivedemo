﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager Instance;
    public GameObject scenePage;
    public GameObject shaderPage;
    public GameObject urlPage;
    public CanvasGroup canvasGroup;
    public GameObject arButton;
    public GameObject fullScreenButton;
    public GameObject OpenFileButton;

    public bool scenePageShow;
    public bool shaderPageShow;
    public bool urlPageShow;
    public bool allUIShow;
    public bool isChinese;
    public bool enableUI = true;

    public CanvasGroup CanvasGroup;
    public Text text1;
    public Text text2;
    public bool toastShow;
    public float toastTime;

    public delegate void OnLanguageChanged(string type);
    public static OnLanguageChanged onLanguageChanged = null;
    public void SetupLanguage(string type)
    {
        if (onLanguageChanged != null)
        {
            onLanguageChanged(type);
        }
    }

    public static UIManager GetInstance() {
        return Instance;
    }

    private void Awake()
    {
        Instance = this;
        if (Application.platform == RuntimePlatform.WindowsPlayer|| Application.platform == RuntimePlatform.WindowsEditor)
        {         
            fullScreenButton.SetActive(true);

            var winComp = fullScreenButton.AddComponent<WindowsForm>();
            fullScreenButton.GetComponent<Button>().onClick.AddListener(()=> {
                winComp.OnClickWindow();
            });

            OpenFileButton.SetActive(true);
        }
        else {
            arButton.SetActive(true);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadDefaultLang();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            OnClickEnableUI();
        }

        if (toastShow) {
            if (toastTime > 15) {
                ShowToast2();
            }
            else {
                toastTime += Time.deltaTime;
            }        
        }
    }

    public void OnClickShaderButton()
    {
        if (shaderPageShow)
        {
            shaderPage.transform.DOLocalMoveX(960, 0.2f);
        }
        else
        {
            shaderPage.transform.SetAsLastSibling();
            shaderPage.transform.DOLocalMoveX(660, 0.2f);
        }
        shaderPageShow = !shaderPageShow;
    }

    public void OnClickSceneButton() {
        
        if (scenePageShow)
        {
            scenePage.transform.DOLocalMoveX(960,0.2f);
        }
        else {
            scenePage.transform.SetAsLastSibling();
            scenePage.transform.DOLocalMoveX(660,0.2f);
        }
        scenePageShow = !scenePageShow;
    }

    public void ShowToast1() {
        toastShow = true;
        CanvasGroup.alpha = 1;
        CanvasGroup.blocksRaycasts = true;
        text1.DOFade(1,0);
        text2.DOFade(0, 0);
    }

    public void ShowToast2()
    {
        MeshPlayerController.GetInstance().PauseVideo();
        toastShow = false;
        CanvasGroup.alpha = 1;
        CanvasGroup.blocksRaycasts = true;
        text2.DOFade(1, 0);
        text1.DOFade(0, 0);
        Invoke("HideToast",2);
        toastTime = 0;
    }

    public void HideToast() {
        CanvasGroup.alpha = 0;
        CanvasGroup.blocksRaycasts = false;
        text2.DOFade(0, 0);
        text1.DOFade(0, 0);
        toastShow = false;
        toastTime = 0;
    }



    public void OnClickUrlButton()
    {

        if (urlPageShow)
        {
            urlPage.transform.DOLocalMoveX(960, 0.2f);
        }
        else
        {
            urlPage.transform.SetAsLastSibling();
            urlPage.transform.DOLocalMoveX(300, 0.2f);
        }
        urlPageShow = !urlPageShow;
    }

    public void LoadDefaultLang()
    {
        string lang = PlayerPrefs.GetString("lang");
        if (!string.IsNullOrEmpty(lang))
        {
            SetupLanguage(lang);
            if (lang == "en") {
                isChinese = false;
            }
            else if (lang == "cn") {
                isChinese = true;
            }
        }
        else {
            OnClickLanguageChange();
        }
    }

    public void OnClickLanguageChange() {
        if (isChinese)
        {
            SetupLanguage("en");
            PlayerPrefs.SetString("lang", "en");
        }
        else {
            SetupLanguage("cn");
            PlayerPrefs.SetString("lang", "cn");
        }

        isChinese = !isChinese;
    }

    public void OnClickEnableUI()
    {
        if (enableUI)
        {
            canvasGroup.alpha = 0;
        }
        else
        {
            canvasGroup.alpha = 1;
        }
        enableUI = !enableUI;
    }
}
