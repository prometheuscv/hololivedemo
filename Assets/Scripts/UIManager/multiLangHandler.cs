﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class multiLangHandler : MonoBehaviour
{

    [SerializeField, Header("Langkey")]
    private string LangKey = "";

    void Awake()
    {
        UIManager.onLanguageChanged += onLanguageChanged;
    }

    private void OnDestroy()
    {
        UIManager.onLanguageChanged -= onLanguageChanged;
    }

    void onLanguageChanged(string languageType)
    {
        Text text = this.gameObject.GetComponent<Text>();
        if (text != null)
        {
            var value = multiLangConst.GetValue(languageType, LangKey);
            text.text = value;
            return;
        }
    }
}