﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Runtime.InteropServices;
//using UnityEngine;

//public class WindowsForm : MonoBehaviour
//{
//    bool isMaxed;
//    // Start is called before the first frame update
//    void Start()
//    {
        
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        if (Input.GetKeyDown(KeyCode.F12)) {
//            SwitchMaximize();
//            //if (isMaxed)
//            //{
//            //    //OnClickRestore();
//            //}
//            //else {
//            //    OnClickMaximize();
//            //}
//            //isMaxed = !isMaxed;
//        }
//    }

//    public void SwitchMaximize() {

//        if (isMaxed)
//        {
//            Screen.SetResolution(1024,768, false);
//        }
//        else
//        {
//            Screen.SetResolution(Screen.width, Screen.height, true);
//        }
//        isMaxed = !isMaxed;
//    }

//    [DllImport("user32.dll")]
//    public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

//    [DllImport("user32.dll")]
//    static extern IntPtr GetForegroundWindow();

//    const int SW_SHOWMINIMIZED = 2; //{最小化, 激活}
//    const int SW_SHOWMAXIMIZED = 3;//最大化
//    const int SW_SHOWRESTORE = 1;//还原
//    public void OnClickMinimize()
//    {
//        //最小化 
//        ShowWindow(GetForegroundWindow(), SW_SHOWMINIMIZED);
//    }

//    public void OnClickMaximize()
//    {
//        //最大化
//        ShowWindow(GetForegroundWindow(), SW_SHOWMAXIMIZED);
//    }

//    public void OnClickRestore()
//    {
//        //还原
//        ShowWindow(GetForegroundWindow(), SW_SHOWRESTORE);
//    }
//}


using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
public class WindowsForm : MonoBehaviour
{
    bool isMaxed;
    [DllImport("user32.dll")]
    public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow();

    const int SW_SHOWMINIMIZED = 2; //{最小化, 激活}
    const int SW_SHOWMAXIMIZED = 3;//最大化
    const int SW_SHOWRESTORE = 1;//还原

    private void Awake()
    {
        Screen.SetResolution(1024, 768, false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            OnClickWindow();
        }

        if (Input.GetKeyDown(KeyCode.F11))
        {
          
        }

        if (Input.GetKeyDown(KeyCode.F10))
        {
            OnClickMinimize();
        }
    }

    public void OnClickWindow() {
        if (!isMaxed)
        {
            OnClickMaximize();
        }
        else
        {
            OnClickRestore();
        }
        isMaxed = !isMaxed;

    }

    public void OnClickMinimize()
    { //最小化 
        ShowWindow(GetForegroundWindow(), SW_SHOWMINIMIZED);
    }

    public void OnClickMaximize()
    {
        //最大化
        ShowWindow(GetForegroundWindow(), SW_SHOWMAXIMIZED);
    }

    public void OnClickRestore()
    {
        //还原
        ShowWindow(GetForegroundWindow(), SW_SHOWRESTORE);
    }
}
