﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ARSceneManager : MonoBehaviour
{
    public string currentMeshVideoPath;
    public MeshPlayerController meshPlayerController;

    public InputField inputField;
    public string inputStr;

    public GameObject planeFinder;
    MeshRenderer meshRenderer;

    // Start is called before the first frame update
    void Start()
    {
        inputField.onValueChanged.AddListener((v) =>
        {
            // @""""
            inputStr = v.Replace(@"""", "");
            inputField.text = inputStr;
        }
        );
        inputStr = "rtmp://192.168.8.107:1935/live";
        meshRenderer = meshPlayerController.GetComponent<MeshRenderer>();
        Invoke("ResetTrack", 1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            meshPlayerController.PlayOrPause();
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            meshPlayerController.SetMaterial(0);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            meshPlayerController.SetMaterial(1);
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            meshPlayerController.SetMaterial(2);
        }
    }

    //public void OpenFile()
    //{
    //    string path = FileBrowser.OpenSingleFile("mp4");

    //    if (!string.IsNullOrEmpty(path))
    //    {
    //        currentMeshVideoPath = path;
    //        meshPlayerController.OpenSource(currentMeshVideoPath);
    //    }
    //    else
    //    {

    //    }
    //}

    public void OpenInputFile()
    {
        if (!string.IsNullOrEmpty(inputStr))
        {
            currentMeshVideoPath = inputStr;
            meshPlayerController.OpenSource(currentMeshVideoPath);
        }
        else
        {

        }
    }

    public void OpenInputFile1()
    {
        currentMeshVideoPath = "rtmp://52.82.41.226/myapp/live";
        meshPlayerController.OpenSource(currentMeshVideoPath);
    }

    public void OpenInputFile2()
    {
        currentMeshVideoPath = "rtmp://pili-live-rtmp.prometh.xyz/holospace/live";
        meshPlayerController.OpenSource(currentMeshVideoPath);
    }

    public void OpenInputFile3()
    {
        currentMeshVideoPath = "20191201-cage10-set06.mp4";
        meshPlayerController.OpenSource(currentMeshVideoPath);
    }

    public void ToVR()
    {
        SceneManager.LoadScene("VRScene");
    }

    public void ResetTrack()
    {
        meshRenderer.enabled = false;
        planeFinder.SetActive(true);
    }

    public void StopTrack()
    {
        meshRenderer.enabled = true;
        planeFinder.SetActive(false);
    }
}
