﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAutoRotate : MonoBehaviour
{
    public ModelViewController modelViewController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (modelViewController.isTouched)
        {

        }
        else {
            transform.Rotate(0,0.1f,0);
        }
      
    }
}
