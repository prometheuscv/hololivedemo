﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using prometheus;

public class MeshPlayerController : MonoBehaviour
{
    private static MeshPlayerController Instance;

    public static MeshPlayerController GetInstance() {
        return Instance;
    }

    public string sourceUrl;
    [HideInInspector]
    public MeshPlayerPlugin meshPlayerPlugin;
    public List<Material> materials;
    public Text frame;
    private void Awake()
    {
        Instance = this;
        meshPlayerPlugin = gameObject.GetComponent<MeshPlayerPlugin>();
        //meshPlayerPlugin.mSourceUrl = GetUrlForStreamingAssets(sourceUrl);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        //自动加载StreamingAssets下的内容
        OpenSource(sourceUrl);
    }

    public void OpenSource(string Url)
    {
        if (meshPlayerPlugin.mIsPlaying)
        {
            PauseVideo();
        }
        else
        {

        }
        meshPlayerPlugin.OpenSource(Url);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetMaterial(int mat)
    {
        if (mat == 0)
        {
            meshPlayerPlugin.SetMaterial(materials[0]);
        }
        else if (mat == 1)
        {
            meshPlayerPlugin.SetMaterial(materials[1]);
        }
        else if (mat == 2)
        {
            meshPlayerPlugin.SetMaterial(materials[2]);
        }
    }

    public void PlayOrPause()
    {
        if (meshPlayerPlugin.mIsPlaying)
        {
            PauseVideo();
        }
        else
        {
            PlayVideo();
        }
    }

    public void PlayVideo()
    {
        meshPlayerPlugin.Play();
        meshPlayerPlugin.mIsPlaying = true;
    }

    public void PauseVideo()
    {
        meshPlayerPlugin.Pause();
        meshPlayerPlugin.mIsPlaying = false;
    }
}
