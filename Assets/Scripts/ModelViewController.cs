﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ModelViewController : MonoBehaviour,IDragHandler,IEndDragHandler,IBeginDragHandler
{
    public GameObject model;
    public GameObject modelParent;
    public Transform cameraParent_X;
    public Transform cameraParent_Y;
    private bool scaleChange;
    public bool isTouched;
    public bool isFirstFrameWithTwoTouches;

    private float mouseScrollWheelScale = 0.1f;
    private float rotateSpeed = 0.5f;

    public float scaleMax;
    public float scaleMin;
    
    public float cachedTouchDistance;
    public float cachedAugmentationScale;
  
    public float deltaX;
    private float deltaY;

    public void Start() {

    }



    void Update()
    {
        if (isTouched) { 
        
        }
        if (Input.GetKey(KeyCode.W))
        {
            cameraParent_X.Translate(0, -0.1f, 0);
        }
        if (Input.GetKey(KeyCode.S))
        {
            cameraParent_X.Translate(0, 0.1f, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            cameraParent_X.Translate(0.1f, 0f, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            cameraParent_X.Translate(-0.1f, 0, 0);
        }

        if (Input.touchCount == 2)
        {
            scaleChange = true;
            float currentTouchDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            if (isFirstFrameWithTwoTouches)
            {

                cachedTouchDistance = currentTouchDistance;
                isFirstFrameWithTwoTouches = false;
            }
            float scaleMultiplier = currentTouchDistance / cachedTouchDistance;
            Debug.Log("scaleMultiplier:"+ scaleMultiplier);
            //scaleMultiplier = Mathf.Clamp(scaleMultiplier, scaleMin, scaleMax);
            float scaleAmount = Mathf.Clamp(cachedAugmentationScale * scaleMultiplier, scaleMin, scaleMax) ;
            modelParent.transform.localScale = new Vector3(scaleAmount, scaleAmount, scaleAmount);
        }
        else
        {
            scaleChange = false;
            isFirstFrameWithTwoTouches = true;
            cachedTouchDistance = 0;
            cachedAugmentationScale = modelParent.transform.localScale.x;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (modelParent.transform.localScale.x < scaleMax)
            {
                modelParent.transform.localScale += Vector3.one * mouseScrollWheelScale;
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (modelParent.transform.localScale.x > scaleMin)
            {
                modelParent.transform.localScale -= Vector3.one * mouseScrollWheelScale;
            }
        }

    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        isTouched = true;
      //  deltaX = cameraParent_X.localEulerAngles.y;
        //deltaY = cameraParent_Y.localEulerAngles
    }

    public void OnDrag(PointerEventData eventData)
    {
     
        if (!scaleChange)
        {
            deltaX = eventData.delta.x;
            deltaY -= eventData.delta.y;
            cameraParent_X.localEulerAngles +=new Vector3(0,deltaX*rotateSpeed, 0);
            cameraParent_Y.localEulerAngles =new Vector3(Mathf.Clamp(deltaY,-0,60),0,0);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        isTouched = false;

        //deltaX = cameraParent_X.localEulerAngles.y;
        //deltaY = cameraParent_Y.localEulerAngles
    }
}
