﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant : MonoBehaviour
{
    //language
    public string en_scene_button ="Scene";
    public string en_scene_button_item_1 ="SpaceShip";
    public string en_scene_button_item_2 = "Bar";

    public string en_shader_button = "Shader";
    public string en_shader_button_item_1 = "Default";
    public string en_shader_button_item_2 = "Holo";
    public string en_shader_button_item_3 = "Normal";

    public string en_url_button ="Url";
    public string en_enter_url ="Enter";

    public string cn_hideUI_button ="HideUI";

    public string cn_scene_button = "Scene";
    public string cn_scene_button_item_1 = "SpaceShip";
    public string cn_scene_button_item_2 = "Bar";

    public string cn_shader_button = "Shader";
    public string cn_shader_button_item_1 = "Default";
    public string cn_shader_button_item_2 = "Holo";
    public string cn_shader_button_item_3 = "Normal";

    public string cn_url_button = "Url";
    public string cn_enter_url = "Enter";
}
