﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crosstales.FB;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VRSceneManager : MonoBehaviour
{
    public string currentMeshVideoPath;
    public MeshPlayerController meshPlayerController;

    public InputField inputField;
    public string inputStr;

    public GameObject Scene0;
    public GameObject Scene1;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        inputField.onValueChanged.AddListener((v) =>
        {
            // @""""
            inputStr = v.Replace(@"""", "");
            inputField.text = inputStr;
        }
        );
        inputStr = "rtmp://192.168.8.107:1935/live";
    }

    public void OpenScene0() {
        Scene0.SetActive(true);
        Scene1.SetActive(false);
    }

    public void OpenScene1()
    {
        Scene1.SetActive(true);
        Scene0.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            meshPlayerController.PlayOrPause();
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            meshPlayerController.SetMaterial(0);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            meshPlayerController.SetMaterial(1);
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            meshPlayerController.SetMaterial(2);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            OpenScene0();
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            OpenScene1();
        }
    }

    public void OpenFile()
    {
        meshPlayerController.meshPlayerPlugin.DataInStreamingAssets = false;
        string path = FileBrowser.OpenSingleFile("mp4");

        if (!string.IsNullOrEmpty(path))
        {
            currentMeshVideoPath = path;
            meshPlayerController.OpenSource(currentMeshVideoPath);
        }
        else
        {

        }
    }

    public void OpenInputFile()
    {
        meshPlayerController.meshPlayerPlugin.DataInStreamingAssets = false;
        if (!string.IsNullOrEmpty(inputStr))
        {
            currentMeshVideoPath = inputStr;
            meshPlayerController.OpenSource(currentMeshVideoPath);
        }
        else
        {

        }
    }

    public void OpenInputFile1()
    {
        meshPlayerController.meshPlayerPlugin.DataInStreamingAssets = true;
        currentMeshVideoPath = "rtmp://52.82.41.226/myapp/live";
        meshPlayerController.OpenSource(currentMeshVideoPath);
    }

    public void OpenInputFile2()
    {
        meshPlayerController.meshPlayerPlugin.DataInStreamingAssets = true;
        currentMeshVideoPath = "rtmp://pili-live-rtmp.prometh.xyz/holospace/live";
        meshPlayerController.OpenSource(currentMeshVideoPath);
    }

    public void OpenInputFile3()
    {
        meshPlayerController.meshPlayerPlugin.DataInStreamingAssets = true;
        currentMeshVideoPath = "20191201-cage10-set06.mp4";
        meshPlayerController.OpenSource(currentMeshVideoPath);
    }

    public void ToAR() {
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
            SceneManager.LoadScene("GroundARScene");
        }
        else if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor)
        {
            SceneManager.LoadScene("MakerARScene");
        }
    }
}
